/* How to Hook with Logos
Hooks are written with syntax similar to that of an Objective-C @implementation.
You don't need to #include <substrate.h>, it will be done automatically, as will
the generation of a class list and an automatic constructor.
*/

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>
#import "YFoldView.h"

@interface SBLockScreenView : UIView
- (CGFloat)_percentScrolled;
-(void)scrollViewDidEndScrollingAnimation:(id)scrollView;
@end

extern "C" UIImage* _UICreateScreenUIImage();

UIView *containerView = nil, *rootContainerView = nil;
UIImageView *screenshotView;
UIImage *screenshot = nil;
YFoldView *foldView;

%hook SBLockScreenView
-(void)scrollViewWillBeginDragging:(id)scrollView {
	%orig;
	if (!screenshot){
		screenshot = _UICreateScreenUIImage();
	}
	if (!containerView){
		screenshotView = [[UIImageView alloc] initWithImage:screenshot];
		screenshotView.backgroundColor = [UIColor clearColor];

		containerView = [[UIView alloc] initWithFrame:screenshotView.bounds];
		containerView.backgroundColor = [UIColor clearColor];

		screenshotView.layer.affineTransform = CGAffineTransformMakeScale(-1, 1);

		[containerView addSubview:[screenshotView autorelease]];

		rootContainerView = [[UIView alloc] initWithFrame:containerView.frame];
		rootContainerView.backgroundColor = [UIColor clearColor];
		rootContainerView.userInteractionEnabled = NO;

		[rootContainerView addSubview:containerView];

		[self addSubview:rootContainerView];
		[self bringSubviewToFront:containerView];
		containerView.layer.affineTransform = CGAffineTransformMakeScale(-1, 1);
	}
	if (!foldView){
		foldView = [[YFoldView alloc] initWithView:containerView withWay:yFoldViewLeftToRight andNumberOfFolds:3];
	}
	UIView *foregroundView = (UIView *)[self valueForKey:@"_foregroundLockView"];
	foregroundView.alpha = 0;
}

-(void)scrollViewDidScroll:(id)scrollView {
	%orig;
	CGFloat x = [scrollView contentOffset].x;
	CGFloat width = [scrollView contentSize].width/2.0f;
	CGFloat offset = x;
	[foldView verticalFoldWithWidth:offset andForce:YES];
	if (offset == width)
		[self scrollViewDidEndScrollingAnimation:scrollView];

	UIView *passcodeView = (UIView *)[self valueForKey:@"_passcodeView"];
	CGRect frame = passcodeView.frame;
	frame.origin.x = x;
	passcodeView.frame = frame;

	CGRect rootFrame = rootContainerView.frame;
	rootFrame.origin.x = (width - x)/5.0f;
	rootContainerView.frame = rootFrame;
}

-(void)_layoutGrabberView:(UIView *)view atTop:(BOOL)top {
	%orig;
	view.alpha = 0;
}

-(void)scrollViewDidEndScrollingAnimation:(id)scrollView {
	%orig;
	UIView *foregroundView = (UIView *)[self valueForKey:@"_foregroundLockView"];
	foregroundView.alpha = 1;

	[containerView removeFromSuperview];
	[containerView release];
	containerView = nil;

	[rootContainerView removeFromSuperview];
	[rootContainerView release];
	rootContainerView = nil;

	[foldView release];
	foldView = nil;
	[screenshot release];
	screenshot = nil;
}
%end
