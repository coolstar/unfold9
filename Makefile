ARCHS = armv7 arm64
include theos/makefiles/common.mk

TWEAK_NAME = Unfold9
Unfold9_FILES = Tweak.xm YFoldView.m
Unfold9_FRAMEWORKS = UIKit QuartzCore CoreGraphics

include $(THEOS_MAKE_PATH)/tweak.mk

after-install::
	install.exec "killall -9 SpringBoard"
